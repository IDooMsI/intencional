<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignkeyProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreignId('category_id')->constrained()->after('timestamps')->nullable();
            $table->foreignId('color_id')->constrained()->after('category_id')->nullable();
            $table->foreignId('subcategory_id')->constrained()->after('color_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('category_id');
            $table->dropForeign('color_id');
            $table->dropForeign('subcategory_id'); 
        });
    }
}
