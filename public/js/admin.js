function countChars(obj) {
    var maxLength = 300;
    var strLength = obj.value.length;
    var charRemain = (maxLength - strLength);

    if (charRemain < 0) {
        document.getElementById("charNum").innerHTML = '<span style="color: red;">Has exedido el maximo de  ' + maxLength + ' caracteres</span>';
    } else {
        document.getElementById("charNum").innerHTML = charRemain + ' caracteres restantes';
    }
}

function getColorName() {
    var hexadecimal = document.getElementById("input-color").value;
    var color = hexadecimal.slice(1);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var colorJson = JSON.parse(xhttp.responseText);
            var input = document.getElementById("input-name-color");
            var array = [];
            var arrayName = colorJson.name;
            for (var clave in arrayName) {
                array.push(arrayName[clave]);
            }
            var p = '<p style="margin:0">' + array[0] + "</p>";
            input.setAttribute("value", array[0]);
            console.log(input);
        }
    };
    xhttp.open("GET", "https://www.thecolorapi.com/id?hex=" + color, true);
    xhttp.send();
}

var divCarousel = document.getElementById('carouselExampleIndicators');
var divFooter = document.getElementById('footer')
var url = window.location
if (url.href.includes('admin') || url.href.includes('categories') || url.href.includes('colors') || url.href.includes('pointsofsale')) {
    divCarousel.style.display = 'none'
    divFooter.style.display = 'none'
}