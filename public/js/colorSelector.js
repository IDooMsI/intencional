function colorChangerEsmalte(id) {
    var fondo = document.getElementById("fondo-color");
    var color = document.getElementById(id);
    var codeShow = document.getElementById('code-show')
    codeShow.innerHTML = color.id
    fondo.setAttribute('src',color.src)
}

function colorChangerHipo(id) {
    var fondo = document.getElementById("fondo-color-hipo");
    var color = document.getElementById(id);
    var codeShow = document.getElementById('code-show-hipo')
    codeShow.innerHTML = color.id
    fondo.setAttribute('src',color.src)
}

function colorChangerGel(id) {
    var fondo = document.getElementById("fondo-color-gel");
    var color = document.getElementById(id);
    var codeShow = document.getElementById('code-show-gel')
    codeShow.innerHTML = color.id
    fondo.setAttribute('src', color.src)
}

function colorChangerProf(id) {
    var fondo = document.getElementById("fondo-color-profesional");
    var color = document.getElementById(id);
    var codeShow = document.getElementById('code-show-profesional')
    codeShow.innerHTML = color.id
    fondo.setAttribute('src', color.src)
}