<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    public $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
