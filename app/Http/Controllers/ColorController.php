<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;
use App\Category;


class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = Color::all();
        $vac = compact('colors');
        return view('admin.color.index', $vac);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $vac = compact('categories',);
        return view('admin.color.create', $vac);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $season = false;

        if (isset($request['season'])) {
            $season = true;
        }

        $image = $this->createImage($request);
        Color::create([
            'name' => $request['name'],
            'hexadecimal' => $request['hexadecimal'],
            'code' => $request['code'],
            'category_id' => $request['category'],
            'image'=>$image,
            'season'=> $season,
        ]);

        return redirect()->route('colors.index')->with('notice', 'El color '. $request['code'] .' ha sido creado correctamente.');
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $color = Color::find($id);
        $categories = Category::all();
        $vac = compact('color', 'categories',);
        return view('admin.color.edit', $vac);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $color = Color::find($id);

        $name = $color->name;
        if (isset($request['name'])) {
            $name = $request['name'];
        }

        $category = $color->category_id;
        if (isset($request['category'])) {
            $category = $request['category'];
        }

        $hexadecimal = $color->hexadecimal;
        if (isset($request['hexadecimal'])) {
            $hexadecimal = $request['hexadecimal'];
        }

        $code = $color->code;
        if (isset($request['code'])) {
            $code = $request['code'];
        }

        $image = $color->image;
        if (isset($request['image'])) {
            $image = $this->createImage($request);
        }

        $season = false;
        
        if (isset($request['season'])) {
            $season = true;
        }

        $color->update([
            'name' => $name,
            'category_id' => $category,
            'hexadecimal' => $hexadecimal,
            'code' => $code,
            'image'=>$image,
            'season'=>$season,
        ]);

        return redirect()->route('colors.index')->with('notice', 'El color ha sido editado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $color = Color::find($id);
        $color->delete();
        return redirect()->route('colors.index')->with('notice', 'El color ha sido eliminado correctamente.');
    }

    /**
     * @param  null|Request
     */
    public function search(Request $req)
    {
        if ($req['buscador']) {
            $colors = Color::where('name', "like", "%" . $req['buscador'] . "%")->get();
            $vac = compact('colors');
            return view('admin.color.results', $vac);
        }

        $colors = Color::all();
        $vac = compact('colors');
        return view('admin.color.results', $vac);
    }
    
    public function validator(Request $request)
    {
        $rules = [
            'name' => 'required||string|max:50',
            'code' =>  'required|unique:colors|string|max:6',
        ];
        $message = [
            'required' => 'El campo es obligatorio.',
            'unique' => 'El color ya existe en nuestra base.',
            'string' => 'Solo se admiten letras.',
            'max' => 'El campo debe tener menos de :max carateres.',
        ];
        return $this->validate($request, $rules, $message);
    }

    public function createImage(Request $request)
    {
        $file = $request['image'];
        $name = $request->input('code') . "." . $file->extension();
        $path = $file->storeAs('colors', $name, 'public');
        
        return $path;
    }
}
