<?php

namespace App\Http\Controllers;

use App\PointOfSale;
use Illuminate\Http\Request;

class PointOfSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pointsOfSale = PointOfSale::all();
        $vac = compact('pointsOfSale');
        return view('admin.pointsofsale.index', $vac);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pointsOfSale = pointOfSale::all();
        $vac = compact('pointsOfSale');
        return view('admin.pointsofsale.create', $vac);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validator($request);
        if (isset($request['authorized'])) {
            $authorized = true;
        }else{
            $authorized = false;
        }
        PointOfSale::create([
            'name'      => $request['name'],
            'address'   => $request['address'],
            'link'      => $request['link'],
            'authorized'=> $authorized
        ]);

        return redirect()->route('pointsofsale.index')->with('notice', 'El punto de venta ha sido creado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PointOfSale  $pointOfSale
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pointOfSale = pointOfSale::find($id);
        $vac = compact('pointOfSale');
        return view('admin.pointofsale.show', $vac);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PointOfSale  $pointOfSale
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pointOfSale = pointOfSale::find($id);
        $vac = compact('pointOfSale');
        return view('admin.pointsofsale.edit', $vac);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PointOfSale  $pointOfSale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pointOfSale = PointOfSale::find($id);

        $name = $pointOfSale->name;
        if (isset($request['name'])) {
            $name = $request['name'];
        }

        $address = $pointOfSale->address;
        if (isset($request['address'])) {
            $address = $request['address'];
        }

        $link = $pointOfSale->link;
        if (isset($request['link'])) {
            $link = $request['link'];
        }

        if (isset($request['authorized'])) {
            $authorized = true;
        }else{
            $authorized = false;
        }

        $pointOfSale->update([
            'name'      => $name,
            'address'   => $address,
            'link'      => $link,
            'authorized'=> $authorized
        ]);

        return redirect()->route('pointsofsale.index')->with('notice', 'El punto de venta ha sido modificado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PointOfSale  $pointOfSale
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pointOfSale = PointOfSale::find($id);
        $pointOfSale->delete();
        return redirect()->route('pointofview.index')->with('notice', 'El punto de vista ha sido eliminado correctamente');
    }

    /**
     * @param  null|Request
     */
    public function search(Request $req)
    {
        if ($req['buscador']) {
            $pointsOfSale = PointOfSale::where('name', "like", "%" . $req['buscador'] . "%")->get();
            $vac = compact('pointsOfSale');
            return view('admin.pointsofsale.results', $vac);
        }

        $pointsOfSale = PointOfSale::all();
        $vac = compact('pointsOfSale');
        return view('admin.pointsofsale.results', $vac);
    }

    public function validator(Request $request)
    {
        $rules = [
            'name' => 'required|unique:pointsofsale|string|',
            'address' =>  'required|unique:pointsofsale|string',
            'link' =>  'required|string',
        ];
        $message = [
            'required' => 'El campo es obligatorio.',
            'unique' => 'El punto de venta ya existe en nuestra base.',
            'string' => 'Solo se admiten letras.',
            'max' => 'El campo debe tener menos de :max carateres.',
        ];
        return $this->validate($request, $rules, $message);
    }
}
