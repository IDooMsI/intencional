<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;
use App\Category;
use App\PointOfSale;



class PageController extends Controller
{
    public function categoryView($id)
    {
        $categories = Category::all();
        $colors = Color::where('category_id', $id)->get();
        $colorPreset = Color::where('category_id', $id)->inRandomOrder()->limit(1)->get();
        $vac = compact('colors','colorPreset');
        foreach ($categories as $categoria) {
            if ($categoria->id == $id) {
                return view('categories.' . $categoria->name, $vac);
            }
        }
    }
}
