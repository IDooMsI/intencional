<!doctype  html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head >
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Intencional BA</title>
    <link rel="Shortcut Icon" href="{{asset('storage/logo.png')}}" type="image/x-icon" />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/boton.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light shadow" style="background-color:#000">
            <div class="row">
                <div class="col-8 col-md-4 col-lg-3">
                    <a class="navbar-brand" href="{{ url('/#video') }}">
                        <img src="{{asset('/storage/logo.png')}}" class="w-100"  alt="image-logo">
                    </a>
                </div>
                <button class="navbar-toggler ml-auto bg-white mr-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse col-10 col-md-6 p-0" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        @foreach($categories as $category)
                        <li class="nav-item dropdown">
                            <a class="nav-link text-center " style="color:#ffd700" href="{{route('esmalte',['id'=>$category->id])}}">{{ucfirst($category->name)}}</a>
                        </li>
                        @endforeach
                        @auth
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            @if(Auth::user()->admin == 325)
                            <div class="dropdown-menu dropdown-menu-right text-dark bg-light" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" style="color:black !important;" href="{{route('admin')}}">Panel</a>
                                <a class="dropdown-item" style="color:black !important;" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                @endif
                            </div>
                        </li>
                        @endauth
                    </ul>
                </div>
                <form class="input-group input-group-sm my-auto col-12 col-md-4 col-lg-2 p-0" action="" method="get">
                        @csrf
                        @method('POST')
                        <input type="text" name="buscador" class="form-control" placeholder="Buscador de productos">
                        <div class="input-group-prepend">
                            <button class="btn  btn-outline-light"><ion-icon style="color:white" name="search-outline"></ion-icon></button>
                        </div>
                    </form>
            </div>
        </nav>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{asset('storage/carrusel1.png')}}" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('storage/carrusel2.png')}}" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('storage/carrusel3.png')}}" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('storage/carrusel4.png')}}" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('storage/carrusel5.png')}}" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('storage/carrusel6.png')}}" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <main class="py-4">
            @yield('content')
        </main>
        <footer id="footer" class="footer mt-auto pt-2 pb-0" style="background-color: black; display:block">
            <div class="col-12 mx-auto">
                <div class="row justify-content-around">
                    <div
                        class="col-12 p-3 col-md-4 text-center align-self-center">
                        <img class="w-75" src="{{ asset ('storage/logo.png') }}" alt="">
                        <img class="w-50" src="{{ asset ('storage/anmat.png') }}" alt="">
                    </div>
                    <div class="col-12 col-md-3 p-3 text-center align-self-center ">
                        <h6 style="color: rgba(255,255,255, 0.723)"><ion-icon name="logo-facebook"></ion-icon> &nbsp <a href="https://www.facebook.com/intencional.ba/" style="color:#b8a268">@intencional.ba</a></h6>
                        <h6 style="color: rgba(255,255,255, 0.723)"><ion-icon name="logo-instagram"></ion-icon> &nbsp <a href="https://www.instagram.com/intencional.ba/" style="color:#b8a268">@intencional.ba</a></h6>
                        <h6 style="color: rgba(255,255,255, 0.723)"><ion-icon name="planet-outline"></ion-icon> &nbsp <a href="https://www.intencional-ba.com/" style="color:#b8a268">intencional-ba.com</a></h6>
                        <h6 style="color: rgba(255,255,255, 0.723)"><ion-icon name="call-sharp"></ion-icon> &nbsp <a href="" style="color:#b8a268">11-7032-1816</a></h6>
                    </div>
                    <div class="col-12 col-md-5 p-3 text-center align-self-center">
                        <h5 class="text-white">PUNTOS DE VENTA</h5>
                        @foreach ($pointsOfSale as $item)
                            <a class="text-reset m-1" href="{{route('esmalte',['id'=>$category->id])}}"><span style="color:#b8a268">{{$item->name}} - </span></a>
                        @endforeach
                    </div>
                </div>
            </div>
        </footer>

    </div>
    <script src="{{ asset('js/colorSelector.js')}}"></script> 
    <script src="{{ asset('js/admin.js')}}"></script> 
    <script src="{{ asset('js/bws.js')}}"></script> 
    <script src="{{ asset('js/admin.js')}}"></script> 
</body>
</html>
