@extends('layouts.app')
@section('content')
<div class="row w-100 justify-content-center">
        <div id="fondo-img" class="col-5 w-75 px-0" >
            <img zindex="1000" class="w-100" style="position: absolute" id="fondo-color" src="{{ asset('/storage/'.$colorPreset[0]->image)}}" alt="">
            <img zindex="1000" class="w-100" style="position: relative" src="{{asset('/storage/esmalte para uñas.png')}}" alt="">
            <input type="hidden" id="categoria" name="esmalte">
            <div style="position: absolute; top:86.5%; left:53.1%; color: #fff; padding: 0.1% 11%;">
                <h4 id="code-show" value="" style="font-size:1.7vw"></h4>
            </div>
            <div style="position: absolute; top:94%; left:53%; color: #000">
                <h4 style="font-size:1.3vw">El color de tu alma</h4>
            </div>
        </div>
        <div class="col-4">  
            <div class="col-12"><h1>Esmalte para uñas</h1></div>
            <div class="col w-100"> 
                <div class="">
                    <h3>Paleta de colores</h3>
                </div>
                <div class="">
                    @foreach($colors as $color)
                        <img class="col-1 w-100 rounded-circle p-0" id="{{ $color->code }}" onclick="colorChangerEsmalte(this.id)" src="{{asset('storage/'.$color->image)}}" class="{{$color->code}}">
                    @endforeach
                </div>
            </div>
            <div class="col mt-3">
                <div>
                    <h3>Metodos de pago</h3>
                </div>
                <img class="w-100" src="{{asset('/storage/tarjetas.jpg')}}" alt="">
            </div>
            <div class="col-12 col-md-10 mt-auto">
                <a id="bwp" class="boton my-3" onclick="whatsapp()" href="javascript:;">
                    <svg class="icon-arrow before">
                        <use xlink:href="#arrow"></use>
                    </svg>
                    <span class="label">Coordinar compra <i class="fas fa-shopping-cart "></i></span>
                    <svg class="icon-arrow after">
                        <use xlink:href="#arrow"></use>
                    </svg>
                </a>
                <svg id="svg-bwp" style="display: none;">
                    <defs>
                        <symbol id="arrow" viewBox="0 0 35 15">
                            <title>Arrow</title>
                            <path d="M27.172 5L25 2.828 27.828 0 34.9 7.071l-7.07 7.071L25 11.314 27.314 9H0V5h27.172z " />
                        </symbol>
                    </defs>
                </svg>
            </div>
        </div>
   </div>
</div>
<div id="div-descripcion-esmalte" class="my-3 col-12 text-center">
    <h3>Descripcion del producto</h3>
    <div style="border: 1.5px solid grey" class="h-25 col-12 text-center my-3">
        <p style="" id="description-show">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tenetur animi odit inventore molestias quaerat provident enim aliquam laboriosam eveniet sint expedita corrupti, ipsum, a itaque ab delectus, ratione id adipisci!</p>
    </div>
</div>    
@endsection