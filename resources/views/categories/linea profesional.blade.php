@extends('layouts.app')
@section('content')
<div class="row w-100 justify-content-center">
        <div id="fondo-img" class="col-5 w-75 px-0">
            <img zindex="1000" class="w-100" style="position: absolute" id="fondo-color-profesional" src="{{ asset('/storage/'.$colorPreset[0]->image)}}" alt="">
            <img zindex="1000" class="w-100" style="position: relative" src="{{asset('/storage/hipoalergenico uv.png')}}" alt="">
            <input type="hidden" id="categoria" name="linea profesional">
        <div style="position: absolute; top:87%; left:74.1%; color: #fff; padding: 0.1% 11%;">
                <h4 id="code-show-profesional" style="font-size:1.7vw"></h4>
            </div>
        </div>
        <div class="col-4">  
            <div>
                <h1>Linea Profesional</h1>
                <h1><strong>GEL-UV</strong></h1>
            </div>
            <div class="row w-100"> 
                <div class="col">
                    <h3>Paleta de colores</h3>
                </div>
                <div class="col-12">
                    @foreach($colors as $color)
                        <img class="col-1 w-100 rounded-circle p-0" id="{{ $color->id }}" onclick="colorChangerProf(this.id)" src="{{asset('storage/'.$color->image)}}" value="{{$color->hexadecimal}}">
                    @endforeach
                </div>
            </div>
            <div>
                <div>
                    <h3>Metodos de pago</h3>
                </div>
                <img class="w-100" src="{{asset('/storage/tarjetas.jpg')}}" alt="">
            </div>
            <div class="col-12 col-md-10 mt-auto">
                <a id="bwp" class="boton my-3" onclick="whatsapp()" href="javascript:;">
                    <svg class="icon-arrow before">
                        <use xlink:href="#arrow"></use>
                    </svg>
                    <span class="label">Coordinar compra <i class="fas fa-shopping-cart "></i></span>
                    <svg class="icon-arrow after">
                        <use xlink:href="#arrow"></use>
                    </svg>
                </a>
                <svg id="svg-bwp" style="display: none;">
                    <defs>
                        <symbol id="arrow" viewBox="0 0 35 15">
                            <title>Arrow</title>
                            <path d="M27.172 5L25 2.828 27.828 0 34.9 7.071l-7.07 7.071L25 11.314 27.314 9H0V5h27.172z " />
                        </symbol>
                    </defs>
                </svg>
            </div>
        </div>
</div>
<div id="div-descripcion-esmalte" class="my-3 col-12 text-center">
    <h3>Descripcion del producto</h3>
    <div style="border: 1.5px solid grey" class="h-25 col-12 text-center my-3">
        <p style="" id="description-show">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tenetur animi odit inventore molestias quaerat provident enim aliquam laboriosam eveniet sint expedita corrupti, ipsum, a itaque ab delectus, ratione id adipisci!</p>
    </div>
</div>  
<div>
    <div class="col-12 text-center">
        <h1>Esmalte para uñas Stamping</h1>
        <img src="{{ asset('storage/TAMPING CORREGIDO.png') }}" alt="">
    </div>
    <div class="col-12 mt-4 text-center">
        <h1>TOPS y Gel Nivelador</h1>
        <img src="{{ asset('storage/LINEA UV.png') }}" alt="">
    </div>
    <div class="col-12 mt-4 text-center">
        <h1>Removedor de esmaltes semipermanente y polish gel</h1>
        <img src="{{ asset('storage/EMOVEDOR.png') }}" alt="">
    </div>
    <div class="col-12 mt-4 text-center">
        <h1>Mascaras Sanitarias</h1>
        <img src="{{ asset('storage/MASCARA SANITARA 2.png') }}" alt="">
    </div>
</div>
@endsection
