@extends('layouts.app')
@section('content')
<div class="row w-100 justify-content-center">
        <div id="fondo-img" class="col-5 w-75 px-0" >
            <img zindex="1000" class="w-100" style="position: absolute" id="fondo-color" src="{{ asset('/storage/'.$colorPreset[0]->image)}}" alt="">
            <img zindex="1000" class="w-100" style="position: relative" src="{{asset('/storage/esmalte.png')}}" alt="">
            <input type="hidden" id="categoria" name="esmaltes">
            <div style="position: absolute; top:85.5%; left:47.1%; color: #fff; padding: 0.1% 11%;">
                <h4 style="font-size:1.7vw"></h4>
            </div>
        </div>
        <div class="col-4">  
            <div><h1>Esmalte para uñas</h1></div>
            <div id="div-descripcion-esmalte">
                <h2>Descripcion</h2>
                <div style="border: 1.5px solid grey" class="h-25 col-md-12 col-12 my-3">
                <p style="" id="description-show">
                    Aca iria la descripcion general del esmalte
                </p>
                </div>
            </div>
            <div class="row w-100"> 
                <div class="col">
                    <h3>Paleta de colores</h3>
                </div>
                <div class="col-12">
                    @foreach($colors as $color)
                        <img class="col-1 w-100 rounded-circle p-0" id="{{ $color->id }}" onclick="colorChanger(this.id)" src="{{asset('storage/'.$color->image)}}" value="{{$color->hexadecimal}}">
                    @endforeach
                </div>
            </div>
            <div>
                <h3>Metodos de pago</h3>
                <p>Imagen con los metodos de pago</p>
            </div>
        </div>
   </div>
@endsection