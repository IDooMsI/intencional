@extends('layouts.app')
@section('content')
<div class="col-12 mx-auto">
    <h1 class="text-center mb-4">Agentes Autorizados</h1>
    <div class="row w-100 mx-auto justify-content-right">
        @foreach ($pointsOfSale as $item)
            @if ($item->authorized)
                <div id="{{ $item->name }}" class="col-12 col-md-6 w-75 col-xl-4 my-3 mx-auto">
                    <h4 class="text-center">{{$item->name}}.</h4>
                    <p class="text-muted text-center">{{$item->address}}.</p>
                    <div class="w-100 row justify-content-center">{!!$item->link!!}</div>
                </div>    
            @endif
        @endforeach
    </div>
    <h1 class="text-center mb-4">Puntos de venta</h1>
    <div class="row w-100 mx-auto justify-content-right">
        @foreach ($pointsOfSale as $item)
            @if (!$item->authorized)
                <div id="{{ $item->name }}" class="col-12 col-md-6 w-75 col-xl-4 my-3 mx-auto">
                    <h4 class="text-center">{{$item->name}}.</h4>
                    <p class="text-muted text-center">{{$item->address}}.</p>
                    <div class="w-100 row justify-content-center">{!!$item->link!!}</div>
                </div>    
            @endif
        @endforeach
    </div>
</div>
@endsection
