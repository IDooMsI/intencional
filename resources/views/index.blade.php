@extends('layouts.app')
@section('content')
<div class="col-12 px-0">
    <div class="row w-100 mx-auto justify-content-center">
        <div class="col-10 mx-auto">
            <video id="video" autoplay muted width="100%" poster="{{ asset('storage/imagen-de-video.jpg') }}" alt="El video no se encuentra disponible">
                <source src="{{ asset('storage/video.mp4')}}" type="video/mp4" />
                <source src="{{ asset('storage/videoogg.ogg')}}" type="video/ogg" />
                <source src="{{ asset('storage/video.webm')}}" type="video/webm " />
                <h4>Tu navegador no soporta este video</h4>
            </video>
        </div>
        <div class="col-12 ">
            <h2 class="text-center">COLORES DE TEMPORADA</h2>
        </div>
        <div class="col-lg-4 col-12 text-center">
            <h3>Hipoalergenico gel</h3>
            <div class="mb-4 col-12">
                @foreach ($colors as $color)
                @if ($color->category->name == 'hipoalergenico gel')
                <img class="col-1 btn w-100 rounded-circle p-0" id="{{ $color->code }}" onclick="colorChangerGel(this.id)" src="{{asset('storage/'.$color->image)}}" value="{{$color->hexadecimal}}">
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-4 col-12 text-center">
            <h3>Hipoalergenico</h3>
            <div class="mb-4 col-12">
                @foreach ($colors as $color)
                @if ($color->category->name == 'hipoalergenico')
                <img class="col-1 btn w-100 rounded-circle p-0" id="{{ $color->code }}" onclick="colorChangerGel(this.id)" src="{{asset('storage/'.$color->image)}}" value="{{$color->hexadecimal}}">
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-4 col-12 text-center">
            <h3>Esmalte para uñas</h3>
            <div class="mb-4 col-12">
                @foreach ($colors as $color)
                @if ($color->category->name == 'esmalte')
                <img class="col-1 btn w-100 rounded-circle p-0" id="{{ $color->code }}" onclick="colorChangerGel(this.id)" src="{{asset('storage/'.$color->image)}}" value="{{$color->hexadecimal}}">
                @endif
                @endforeach
            </div>
        </div>
        <div id="fondo-season" class="mt-5 col-lg-5 col-7 w-100 px-0" >
            <img zindex="1000" class="w-100" style="position: absolute" id="fondo-color-gel" src="" alt="">
            <img zindex="1000" class="w-100" style="position: relative" src="{{asset('/storage/colores de estacion.png')}}" alt="">
            <input type="hidden" id="categoria" name="">

            <div style="position: absolute; top:86.5%; left:53.1%; color: #fff; padding: 0.1% 11%;">
                <h4 id="code-show-gel" style="font-size:1.7vw"></h4>
            </div>
        </div>
    </div>
</div>
@endsection
