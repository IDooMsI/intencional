@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="text-center col-12">
            <h2>Editar Categoria</h2>
        </div>
        <form class="col-12 text-center mx-auto" action="{{route('categories.update',['category'=>$category])}}" method="POST">
            @method('PUT')
            @csrf
            <div class="mb-3 col-12">
                <div class="col-12">
                    <h4><label for="">Nombre</label></h4>
                </div>
                <input type="text" name="name" value="{{$category->name}}">
                @error('name')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-3 mt-2"><span>{{ $message }}</span></div>
                @enderror
            </div>
            {{-- <div id="" class="mb-3 col-12">
                <div class="col-12">
                    <h4><label for="">Descripcion</label></h4>
                </div>
                <textarea name="description" id="" cols="30" rows="5" value="">{{$category->description}}</textarea>
                @error('description')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-3 mt-2"><span>{{ $message }}</span></div>
                @enderror
            </div> --}}
            <div class="mx-auto col-4">
                <button class="btn btn-outline-dark" type="submit">Editar</button>
                <a href="{{route('categories.index')}}"><button class="btn btn-outline-dark" type="button">Cancelar</button></a>
            </div>
        </form>
    </div>
</div>
@endsection