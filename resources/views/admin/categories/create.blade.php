@extends('layouts.app')
@section('content')
<div class="col-12">
    <div class="text-center col-12">
        <h2>Nueva Categoria</h2>
    </div>
    <form class="col-12 text-center mx-auto" action="{{route('categories.store')}}" method="post">
        @csrf
        <div class="mb-3 col-12">
            <div class="col-12">
                <h4><label for="">Nombre</label></h4>
            </div>
            <input name="name" class="" type="text">
            @error('name')
            <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
            @enderror
        </div>
        <div class="mb-3 col-12">
            <div class="col-12">
                <h4><label class="" for="">Descripcion</label></h4>
            </div>
            <textarea class="" name="description" maxlength="300" cols="30" rows="5" onkeyup="countChars(this);" placeholder="Maximo 300 caracteres"></textarea>
            <p id="charNum">300 caracteres restantes</p>
            @error('description')
            <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
            @enderror
        </div>
        <div class="col-12">
            <button class="mx-2 btn btn-outline-dark" type="submit">Agregar</button>
            <a href="{{route('categories.index')}}"><button class="btn btn-outline-dark" type="button">Cancelar</button></a>
        </div>
    </form>
</div>
@endsection