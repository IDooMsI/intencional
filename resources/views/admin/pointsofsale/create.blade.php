@extends('layouts.app')
@section('content')
<div class="col-12">
    <div class="col-12 text-center mb-5">
        <h2>Nuevo punto de venta</h2>
    </div>
    <form class="col-12 mx-auto" action="{{route('pointsofsale.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-row">
            <div id="div-name" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Nombre</label></h4>
                <input name="name" class="form-control" type="text" value="{{ old('name') }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Nombre del punto de venta</b></small>
                @error('name')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="div-address" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Direccion</label></h4>
                <input name="address" class="form-control" type="text" value="{{ old('address') }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Direccion del punto de venta</b></small>
                @error('address')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="div-link" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Link del mapa</label></h4>
                <input name="link" class="form-control" type="text" value="{{ old('link') }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Link extraido de maps</b></small>
                @error('link')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="div-authorized" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Autorizado</label></h4>
                 <div class="form-check">
                    <label for="">si</label>
                    <input name="authorized" class="mt-1 mr-1" type="checkbox" value="si">
                </div>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Marcar este campo solo si el punto de venta es agente autorizado</b></small>
                @error('authorized')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
        </div>
        <div id="buttons" class="text-center col-12">
            <button class="mx-2 btn btn-outline-dark" type="submit">Agregar</button>
            <a href="{{route('pointsofsale.index')}}"><button class="btn btn-outline-dark" type="button">Cancelar</button></a>
        </div>
    </form>
</div>
@endsection