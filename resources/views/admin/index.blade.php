@extends('layouts.app')
@section('content')
<div class="container">
    <div id="div-admin-buttons" class="row mt-3 justify-content-center">
        <div class="col-2">
            <a href="{{route('colors.index')}}"><button class="btn btn-outline-dark">Colores</button></a>
        </div>
        <div class="col-2">
            <a href="{{route('categories.index')}}"><button class="btn btn-outline-dark">Categorias</button></a>
        </div>
         <div class="col-2">
            <a href="{{route('pointsofsale.index')}}"><button class="btn btn-outline-dark">Puntos de venta</button></a>
        </div>
    </div>
</div>
@endsection