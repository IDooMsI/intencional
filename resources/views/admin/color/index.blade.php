@extends('layouts.app')
@section('content')
<div id="div-all-color" class="col-12">
    <div id="div-tit-color" class="mx-auto text-center col-2">
        <h2>Colores</h2>
        <a href="{{route('admin')}}" style="text-decoration: none; color:black"><i class="fas fa-arrow-circle-left"></i> Volver</a>
    </div>
    <div id="div-opc-color" class="row justify-content-center mx-auto mb-3 col-12 col-md-8 col-lg-6 col-xl-4">
        <div class="my-3 col-6 col-md-4">
            <a href="{{route('colors.create')}}"><button class="btn btn-outline-dark w-100">Nuevo</button></a>
        </div>
        <form class="input-group input-group-sm mb-5 my-auto col-12 col-md-6" action="{{route('colors.search')}}" method="post">
            @csrf
            <div class="input-group-prepend">
                <button class="input-group-text" id="inputGroup-sizing-sm">Buscar</button>
            </div>
            <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
        </form>
    </div>
    <div class="my-2 col-12 text-center">
        @if(Session::has('notice'))
        <h3 class="my-auto text-success"><strong>{{ Session::get('notice') }}</strong></h3>
        @endif
    </div>
</div>
<div class="table" style="overflow-x:auto;">
    <table class="mx-auto">
        <thead>
            <tr class="text-center">
                <th scope="col">#</th>
                <th scope="col">Categoria</th>
                <th scope="col">Temporada</th>
                <th scope="col">imagen</th>
                <th scope="col">Codigo</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($colors))
            @foreach($colors as $color => $data)
            <tr class="text-center">
                <th scope="row">{{$data->id}}</th>
                <th>{{$data->category->name}}</th>
                @if ($data->season)
                <th class="text-center">Si</th>
                @else
                <th class="text-center">No</th>
                @endif
                <th class="w-25"><img class="col-1 w-100 rounded-circle p-0" id="{{ $data->id }}" src="{{asset('storage/'.$data->image)}}"></>
                <th>{{$data->code}}</th>
                <th>
                    <a href="{{route('colors.edit',['color'=>$data])}}">editar</a>
                    ||
                    <a href="{{route('colors.delete',['id'=>$data])}}">eliminar</a>
                </th>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection