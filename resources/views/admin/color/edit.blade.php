@extends('layouts.app')
@section('content')
<div class="col-12">
    <div class="col-12 text-center mb-5">
        <h2>Modificar Color</h2>
    </div>
    <form class="col-12 mx-auto" action="{{route('colors.update',['color'=>$color])}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-row">
             {{-- <div id="div-color" class="form-group col-12 col-md-6 col-lg-1">
                <h4><label for="">Color</label></h4>
                <input class="" id="input-color" name="hexadecimal" type="color" value="{{ $color->hexadecimal}}">
                @error('color')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div> --}}
            <div id="category" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Categoria</label></h4>
                <select class="form-control mb-1" name="category" value="{{ $color->category->name }}" id="">
                    <option value="">Elija una categoria</option>
                    @foreach($categories as $category)
                    <option <?php if ($category->id == $color->category_id) echo "selected"; ?> value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Categoria a la que pertenece el color</b></small>
                <a href="{{route('categories.create')}}"><button class="btn btn-outline-dark" type="button">Agregar Categoria</button></a>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Si no encuentra la categoria en el listado, agreguela</b></small>
                @error('category_id')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="code" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Codigo</label></h4>
                <input name="code" class="form-control" type="text" value="{{ $color->code }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Codigo del color</b></small>
                @error('code')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
             {{-- <div id="hexadecimal" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Valor Hexadecimal</label></h4>
                <input name="hexadecimal" class="form-control" type="text" value="{{ $color->hexadecimal }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Codigo del valor hexadecimal</b></small>
                @error('code')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div> --}}
            <div id="image" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Imagen</label></h4>
                <input name="image" class="form-control-file" type="file">
                @error('image')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="extras" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Extras</label></h4>
                <div class="form-check">
                    @if ($color->season)
                    <input name="season" class="mt-1 mr-1" type="checkbox" checked value="si">
                    <label for="">Color de temporada</label>
                    @else
                    <input name="season" class="mt-1 mr-1" type="checkbox" value="si">
                    <label for="">Color de temporada</label>
                    @endif
                </div>
            </div>
        </div>
        <div id="buttons" class="text-center col-12">
            <button class="mx-2 btn btn-outline-dark" type="submit">Modificar</button>
            <a href="{{route('colors.index')}}"><button class="btn btn-outline-dark" type="button">Cancelar</button></a>
        </div>
    </form>
</div>
@endsection