@extends('layouts.app')
@section('content')
<div class="col-12">
    <div class="col-12 text-center mb-5">
        <h2>Nuevo Color</h2>
    </div>
    <form class="col-12 mx-auto" action="{{route('colors.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-row">
            <div id="category" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Categoria</label></h4>
                <select class="form-control mb-1" name="category" value="{{ old('category') }}" id="">
                    <option value="">Elija una categoria</option>
                    @if(isset($categories))
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    @endif
                </select>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Categoria a la que pertenece el color</b></small>
                <a href="{{route('categories.create')}}"><button class="btn btn-outline-dark" type="button">Agregar Categoria</button></a>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Si no encuentra la categoria en el listado, agreguela</b></small>
                @error('category_id')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="div-code" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Codigo</label></h4>
                <input name="code" class="form-control" type="text" value="{{ old('code') }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Codigo del color</b></small>
                @error('code')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="hexadecimal" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Valor Hexadecimal</label></h4>
                <input name="hexadecimal" class="form-control" type="text" value="#{{ old('hexadecimal') }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Codigo del valor hexadecimal</b></small>
                @error('code')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="div-image" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Imagen</label></h4>
                <input name="image" class="form-control-file" type="file">
                @error('image')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="extras" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Extras</label></h4>
                <div class="form-check">
                    <input name="season" class="mt-1 mr-1" type="checkbox" value="si">
                    <label for="">Color de temporada</label>
                </div>
            </div>
        </div>
        <div id="buttons" class="text-center col-12">
            <button class="mx-2 btn btn-outline-dark" type="submit">Agregar</button>
            <a href="{{route('colors.index')}}"><button class="btn btn-outline-dark" type="button">Cancelar</button></a>
        </div>
    </form>
</div>
@endsection