<?php

use Illuminate\Support\Facades\Route;
use App\Category;
use App\PointOfSale;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', function () {
    return view('admin/index');
})->name('admin');


Route::get('/categories/{id}/delete', 'CategoryController@destroy')->name('categories.delete');
Route::resource('categories', 'CategoryController');

Route::get('/colors/{id}/delete', 'ColorController@destroy')->name('colors.delete');
Route::resource('colors', 'ColorController');
Route::post('/colors/search', 'CategoryController@search')->name('colors.search');

Route::get('/pointsofsale/{id}/delete', 'PointOfSaleController@destroy')->name('pointsofsale.delete');
Route::resource('pointsofsale','PointOfSaleController');
Route::post('/pointsofsale/search', 'PointOfSaleController@search')->name('pointsofsale.search');


Route::resource('offers', 'OfferController');
Route::resource('products', 'ProductController');
Route::resource('subcategories', 'SubcategoryController');

// Route::get('/hipoalergenicos', 'ColorController@showHipo');




View::composer('*', function ($view) {
    $categories = Category::all();
    $view->with('categories', $categories);
});

View::composer(['layouts.app', 'categories.puntos de venta'], function ($view) {
    $pointsOfSale = PointOfSale::all();
    $view->with('pointsOfSale', $pointsOfSale);
});

View::composer('categories.puntos de venta', function ($view) {
    $pointsOfSale = PointOfSale::all();
    $view->with('pointsOfSale', $pointsOfSale);
});

Route::get('/seccion/esmalte/{id}', 'PageController@categoryView')->name('esmalte');